#ifndef LISTE_HPP
#define LISTE_HPP

struct Noeud {
    int _valeur;
    Noeud * _suivant=nullptr;
};

struct Liste {
    Noeud * _tete;
    Liste();
    ~Liste();
    void ajouterDevant(int valeur);
    const int getTaille();
    const int getElement(int indice);
};

#endif // LISTE_HPP

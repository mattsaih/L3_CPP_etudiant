#include "liste.hpp"
#include <iostream>

Liste::Liste(){
    _tete = nullptr;
}

Liste::~Liste(){
    while(_tete->_suivant==nullptr){
        Noeud * tmp = _tete;
        delete _tete;
        _tete = tmp -> _suivant;
    }
}

void Liste::ajouterDevant(int valeur){
    if(_tete==nullptr){
     _tete=new Noeud;
     _tete->_valeur=valeur;
     _tete->_suivant=nullptr;
    }
    Noeud * tmp = new Noeud;
    tmp->_valeur=valeur;
    tmp->_suivant=_tete;
    _tete=tmp;

}

const int Liste::getTaille(){
    if(_tete==nullptr) return 0;
    int i=0;
    while(_tete->_suivant!=nullptr){
        i++;
        _tete = _tete->_suivant;
    }
    return i;
}

const int Liste::getElement(int indice){
    if(getTaille()<indice){
       std::cout<<"Indice invalide"<<std::endl;
       return -1;
    }
    for(int i = 1; i < indice; i++){
        _tete = _tete->_suivant;
    }
    return _tete->_valeur;

}

#ifndef FIBONNACCI_HPP_
#define FIBONNACCI_HPP_

int fibonnacciRecursif(int n);
int fibonnacciIteratif(int n);


#endif

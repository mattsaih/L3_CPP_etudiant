#include <iostream>
#include "Fibonnacci.hpp"
#include "vecteur3.hpp"

int main(){
    std::cout<<"Fibonnacci recursif :"<<fibonnacciRecursif(7)<<std::endl;
	std::cout<<"Fibonnacci iteratif :"<<fibonnacciIteratif(7)<<std::endl;

    struct Vecteur3D a = {2,3,6};
    afficher(a);
    a.afficher();
    std::cout<<"Norme : "<<a.norme()<<std::endl;
	return 0;
}

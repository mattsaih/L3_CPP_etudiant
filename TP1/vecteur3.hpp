#ifndef VECTEUR3_HPP
#define VECTEUR3_HPP

struct Vecteur3D {
    float x;
    float y;
    float z;

    void afficher();
    double norme();
};

void afficher(const struct Vecteur3D &a);

double produitScalaire(const struct Vecteur3D &a, const struct Vecteur3D &b);

#endif // VECTEUR3_HPP

#include "vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, test_norme) {
    struct Vecteur3D a = { 2,3,6, };
    int result = a.norme();
    CHECK_EQUAL(7, result);
}

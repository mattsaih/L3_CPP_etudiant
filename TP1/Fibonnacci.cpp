#include "Fibonnacci.hpp"

int fibonnacciRecursif(int n){
	if(n<2) return n;
	else{
		return fibonnacciRecursif(n-1) + fibonnacciRecursif(n-2);
	}
}
	
int fibonnacciIteratif(int n){
	if(n<2) return n;
	else{
		int F = 0;
		int p = 0;
		int d = 1;
		for(int i=2; i <= n; i++){
			F = p+d;
			p = d;
			d = F;
		}
		return F;
	}	
}

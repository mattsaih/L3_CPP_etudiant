#include "Fibonnacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonnacci) { };

TEST(GroupFibonnacci, test_fibonnacciRecursif) {
    int result = fibonnacciRecursif(5);
    CHECK_EQUAL(5, result);
}

TEST(GroupFibonnacci, test_fibonnacciIteratif) {
    int result = fibonnacciIteratif(5);
    CHECK_EQUAL(5, result);
}

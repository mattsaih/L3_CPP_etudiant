#include "Image.hpp"
#include <fstream>
#include <cmath>

Image::Image(int largeur, int hauteur) :
	_largeur(largeur), _hauteur(hauteur) {
		_pixels = new int[_largeur * _hauteur];
		for(int i = 0; i < _largeur * _hauteur; i++){
			_pixels[i] = 0;
		}
}

Image::Image(const Image & img) :
	_hauteur(img._largeur), _largeur(img._hauteur) {
		_pixels = new int [_largeur * _hauteur];
		
		for(int k=0; k<_largeur*_hauteur; k++)
		_pixels[k] = img._pixels[k]; 
}

Image::~Image() {
	delete [] _pixels;
}

const Image & Image::operator=(const Image & img){
	this -> Image(img);
	return *this;
}

int Image::getLargeur() const {
	return _largeur;
}

int Image::getHauteur() const {
	return _hauteur;
}

int Image::getPixel(int i, int j) const {
	return _pixels[i*_largeur + j];
}
	
void Image::setPixel(int i, int j, int couleur) {
	_pixels[i*_largeur + j] = couleur;
}


int & Image::pixel(int i, int j, int couleur) { //Modificateur
	_pixels[i*_largeur + j] = couleur;
}

int & Image::pixel(int i, int j) const { //Accesseur
	return _pixels[i*_largeur + j];
}

void ecrirePnm(const Image & img, const std::string & nomFichier) {
	std::ofstream ofs(nomFichier);
	ofs << "P2" << std::endl;
	ofs << img.getLargeur() << " " << img.getHauteur() << std::endl;
	ofs << "255" << std::endl;
	
	for(int i =0; i < img.getHauteur(); i++) {
		for(int j =0; j < img.getLargeur(); j++) {
			ofs << img.pixel(i, j);
		}
		ofs << std::endl;
	}
}

void remplir(Image & img) {
	for (int j = 0; j < img.getLargeur(); j++){
		double t = 2 * M_PI*j / img.getLargeur();
		int c = (cos(t) + 1) *127;
		for(int i = 0; i < img.getLargeur(); i++) {
			img.pixel(i,j) = c;
		}
	}
}

Image bordure(const Image & img, int couleur) {
	Image img2 = img;
	const int i1 = img2.getLargeur() -1;
	for(int i=0; i<img2.getHauteur(); i++){
		img2.pixel(i,0) =couleur;
		img2.pixel(i,i1) = couleur;
	}
	const int j1 = img2.getHauteur() -1;
	for(int j = 0; j<img2.getLargeur(); j++){
		img2.pixel(0,j) = couleur;
		img2.pixel(j1, j) = couleur;
	}
	
	return img2;
	
}

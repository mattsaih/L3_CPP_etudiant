#include "ZoneDessin.hpp"

ZoneDessin::ZoneDessin()
{
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {630,10}));
    _figures.push_back(new Ligne({1,0,0}, {630,10}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,470}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {10,470}));
    _figures.push_back(new PolygoneRegulier({0,1,0}, {100,200}, 50, 5));
}

ZoneDessin::~ZoneDessin()
{
    for(FigureGeometrique* ptr : _figures)
    {
        delete ptr;
    }
}

bool ZoneDessin::on_expose_event(GdkEventExpose* event)
{
    return false;
}

bool ZoneDessin::gererClic(GdkEventButton *event)
{
    return false;
}

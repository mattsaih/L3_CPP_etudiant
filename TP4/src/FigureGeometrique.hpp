#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include "couleur.hpp"

class FigureGeometrique
{

private:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher() const;
};

#endif // FIGUREGEOMETRIQUE_HPP

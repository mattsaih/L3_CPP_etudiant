#include "PolygoneRegulier.hpp"
#include <iostream>
#include <cmath>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : FigureGeometrique (couleur)
{
    _nbPoints = nbCotes;
    _points = new Point [_nbPoints];

    for(int i = 0; i<_nbPoints; i++){
        double alpha = (i * M_PI * 2)/_nbPoints;
        int x = rayon * cos(alpha) + centre._x;
        int y = rayon * sin(alpha) + centre._y;
        _points[i] = {x,y};
    }
}

void PolygoneRegulier::afficher() const {
    std::cout<<"PolygoneRegulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" ";
    for (int i = 0;i < _nbPoints; i++) {
        std::cout<<_points[i]._x<<"_"<<_points[i]._y<<" ";
        if(i == 4) std::cout<<std::endl;
    }
}

int PolygoneRegulier::getNbPoints() const
{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const
{
    return _points[indice];
}

